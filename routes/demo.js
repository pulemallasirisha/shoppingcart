var express = require('express');

var router = express.Router();

var groupscontroller = require('../controller/groupscontroller');

/*GET home page.*/

router.get('/',groupscontroller.getAll);
router.get('/withcategories',groupscontroller.getAllWithCategories);
module.exports = router;