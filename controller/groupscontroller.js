 var Model = require('../models');

module.exports.getAll = function (req, res) {

  Model.Groups.findAll()

    .then(function (groups) {

      res.send(groups);

    })

}

module.exports.getAllWithCategories = function (req, res) {

  Model.Groups.findAll({ include: Model.Categories })

    .then(function (categories) {

      res.send(categories);

    })

}