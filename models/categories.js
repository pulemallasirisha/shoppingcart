'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Categories extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Categories.belongsTo(models.Groups)
      Categories.hasMany(models.Products);
    }
  }
  Categories.init({
    name: DataTypes.STRING,
    descriptio: DataTypes.STRING,
    groupid: DataTypes.INTEGER,
    isactive: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'Categories',
  });
  return Categories;
};