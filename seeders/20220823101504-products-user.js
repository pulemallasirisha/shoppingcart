'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    await queryInterface.bulkInsert('Products', [{
      name: 'lenovo thinkpad',
      description:'Lenovo ThinkPad is a Windows 10 laptop with a 14.00-inch display.',
      image_url:'https://upload.wikimedia.org/wikipedia/commons/4/45/Lenovo_ThinkPad_X1_Ultrabook.jpg',
      categoryId:1,
      isactive:'true',
      createdAt:new Date(),
      updatedAt:new Date()
     },
    {
      name: 'Dell G7 17 (2020)',
      description:'It is powered by a Core i5 processor and it comes with 8GB of RAM.',
      image_url:'https://m.media-amazon.com/images/I/51lujnPZRwL._SX425_.jpg',
      categoryId:2,
      isactive:'true',
      createdAt:new Date(),
      updatedAt:new Date()
    },
    {
      name: 'onepluse nord 2',
      description:'A worthy successor for the first OnePlus Nord on all counts.',
      image_url:'https://boip.in/60385-small_default/oneplus-nord-2-5g-12gb-256gb-refurbished-very-good.jpg',
      categoryId:3,
      isactive:'true',
      createdAt:new Date(),
      updatedAt:new Date()
    },
    {
      name: 'OPPO F19 Pro',
      description:'The Oppo F19 Pro is a decent mid-range smartphone with good display and battery life',
      image_url:'https://i0.wp.com/www.smartprix.com/bytes/wp-content/uploads/2021/03/1.jpg?fit=4032%2C3024&ssl=1',
      categoryId:4,
      isactive:'true',
      createdAt:new Date(),
      updatedAt:new Date()
    },
    {
      name: 'boAt Airdopes 441',
      description:'Airdopes 441 wireless earbuds offer battery backup of up to 5 hours on a single charge',
      image_url:'https://m.media-amazon.com/images/I/51lEIy51L6L._SL1500_.jpg',
      categoryId:5,
      isactive:'true',
      createdAt:new Date(),
      updatedAt:new Date()
    }], {});
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('Products', null, {});
  }
};
