'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    await queryInterface.bulkInsert('Categories', [{
      name:'lenovo',
      descriptio:'Lenovo is one of the worlds leading personal technology companies, producing innovative PCs and mobile internet devices',
      groupid:1,
      isactive:'true',
      createdAt:new Date,
      updatedAt:new Date
    },
    {
      name:'Dell',
      descriptio:'The company designs, develops, manufactures, markets, sells, and supports information technology infrastructure such as laptops, desktops, mobiles,',
      groupid:1,
      isactive:'true',
      createdAt:new Date,
      updatedAt:new Date

    },
    {
      name:'oneplus',
      descriptio:'OnePlus phones are known for their solid battery life and fast charging support,',
      groupid:2,
      isactive:'true',
      createdAt:new Date,
      updatedAt:new Date

    },
    {
      name:'Oppo',
      descriptio:'Oppo phones are smartphones produced by the partially state-owned Chinese company Oppo, running in several countries',
      groupid:2,
      isactive:'true',
      createdAt:new Date,
      updatedAt:new Date
    },
    {
      name:'Boat',
      descriptio:' Most of the earphones from this brand come with simple touch controls to receive phone calls',
      groupid:3,
      isactive:'false',
      createdAt:new Date,
      updatedAt:new Date


    }], {});
  },

  async down (queryInterface, Sequelize) {
    /**
     * 
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     await queryInterface.bulkDelete('Categories', null, {});
  }
};
